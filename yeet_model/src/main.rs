#![allow(incomplete_features)]
#![feature(const_generics)] // This feature is required to use exotic.

use std::fs::read_dir;
use exotic_macro::*;
use exotic::{
    prelude::*,
    deep::DenseLayer,
    deep::activation::*,
    deep::LazyAxon,
    I,
};

use exotic::matrix; 

use image::GenericImageView;
use image::Pixel; 
use rayon::prelude::*;

use rand::random;

const MEDICAL_IMAGE_WIDTH: usize = 64; // Emojis have same height and width, therefore we don't need a const for the height.
const MEDICAL_IMAGE_SIZE: usize = MEDICAL_IMAGE_WIDTH * MEDICAL_IMAGE_WIDTH;
// So we don't need to type MEDICAL_IMAGE_WIDTH * MEDICAL_IMAGE_WIDTH,
// every time we refer to the total pixel count in an medical image.

 
model!((
    name: "Encoder",
    cached: true, // Enabling caching for the model will make it faster, feedforward both for the prediction and for the backpropagation, but can do it once instead.
    layers: [
        ("DenseLayer", [4096, 121]), // First argument to the dense layer is the input size, second is the output size. The input size can't be an identifier (this is an incomplete feature in exotic)
        ("Swish", [121]), // Swish swoosh swash
        ("DenseLayer", [121, 6]),
        ("Tanh", [6]), // Hyberbolic tangence funcition
    ]
));

model!((
    name: "Decoder",
    cached: true,
    serialization: Some(true),
    layers: [
        ("DenseLayer", [6, 121]),
        ("Swish", [121]), // SWiIiIiIiIIiiIiiIiIiiIIiIiiIIIiiIIIIISH cheese!
        ("DenseLayer", [121, 4096]),
        ("Sigmoid", [4096]), // Sigi Sigi Moid
    ]
));

const DATA_SOURCES: &[&str] = &[ // This array contains the directories for the different types of medical images.
//    "AbdomenCT", // We don't need these for now... Maybe some other day.
//    "BreastMRI",
//    "ChestCT"  ,
//    "CXR"      ,
//    "Hand"     ,
    "HeadCT"   ,
];

fn pick_file(max: usize) -> String{ // Pick random filename.
    let n = random::<usize>() % max; // This will limit the n (the file picked), to the last file;
    let n = format!("{}", n);
    format!("{}{}.jpeg","0".repeat(6-n.len()), n) // The file name should have leading zeroes, and a length of 6. Aaaaand then end with '.jpeg'
}

fn main() -> Result<()>{

    //return decode_random();

    let mut encoder = Encoder{ // Set hyperparameters. These we're set manually, but can be done much better with bay's theorem and metagradients. But thats a bit overkill for the task at hand.
        l0: DenseLayer::random(0.001),
        l1: Swish,
        l2: DenseLayer::random(0.001),
        l3: Tanh,
        cache: Encoder::new_cache(),
    };

    let mut decoder = Decoder{
        l0: DenseLayer::random(0.002),
        l1: Swish,
        l2: DenseLayer::random(0.002),
        l3: Sigmoid,
        cache: Decoder::new_cache(),
    };

    decoder.deserialize(&mut std::fs::read("../../yo2.exotic")?.iter().map(|n|*n)); // If we want
    //to continue the learning later, we can uncomment this line.
    
    decoder.l0.lr = 0.002;
    decoder.l2.lr = 0.003;

    let mut max_file = [0; DATA_SOURCES.len()]; // Keeps track of how many files are in each directory. (we only use one)

    for (o, dir) in max_file.iter_mut().zip(DATA_SOURCES.iter()){
        *o = read_dir(format!("./medmnist/{}", dir))?.count() // Count the files...
    }

    for epoch in 0..{
        let mut img_buffer = [0.; MEDICAL_IMAGE_SIZE];
        let category = random::<usize>() % DATA_SOURCES.len(); // Pick random category to pick image from.
        let path = format!("./medmnist/{}/{}", DATA_SOURCES[category], pick_file(max_file[category])); // Pick random image.

        let image = image::open(&path).unwrap(); // Open image and loop over all the pixels (very slow. This would be an easy pick to optimize)
        img_buffer.par_iter_mut().enumerate().for_each(|(n, o)|{
            let x = n % MEDICAL_IMAGE_WIDTH;
            let y = (n - x) / MEDICAL_IMAGE_WIDTH;

            *o = image.get_pixel(x as u32, y as u32).to_luma()[0] as f32 / 255.;
        });

        drop(image); // We don't need these anymore...
        drop(path);
        drop(category);

        let input = LazyAxon::<{Encoder::I_LEN}>(&img_buffer);

        let encoding = encoder.cache(input)?; // Generate encoding (latent representation)
        let decoding = decoder.cache(encoding)?; // Try to decode it again.

        let mut delta = vec![0.; MEDICAL_IMAGE_SIZE]; // Buffer to keep track of how wrong we were.
        let cost: f32 = delta
            .par_iter_mut() // Use rayon to do this in parallel. Easy and fast.
            .zip(decoding.par_iter())
            .zip(img_buffer.par_iter())
            .map(|((d, o), y)|{
                *d = o - y; // Calculate derivative of our output in respect to our cost.
                (o - y).powi(2) // Calculate cost
            })
            .sum(); // And then take the sum of the cost.

        
        let delta = decoder.backpropagate_from_cache(encoding, LazyAxon::<{Decoder::O_LEN}>(&delta[..]))?;
        encoder.backpropagate_from_cache(input, delta)?;

        if epoch % 1000 == 0{ // Print progress every 1000 epoch.
            //println!("cost {}", cost); // Print the cost, so we can see if the model is making progress.
            matrix![&decoder.cache.3, I![const 64, 64]].heatmap(1.)?; // use exotic macro to print the decoded image in the terminal.
        }

        if epoch % 20000 == 0{ // Save every 20000 epoch.
            println!("saving... cost {}", cost);
            decoder.serialize_to(&mut std::fs::File::create("yo3.exotic")?)?;
            println!("Done")
        }
    }

    Ok(())
}

fn decode_random() -> Result<()>{ // Print some random brains to test out model...
    let mut decoder = Decoder{
        l0: DenseLayer::random(0.0001),
        l1: Swish,
        l2: DenseLayer::random(0.0002),
        l3: Sigmoid,
        cache: Decoder::new_cache(),
    };
    decoder.deserialize(&mut std::fs::read("decoder.exotic")?.iter().map(|n|*n));

    for _ in 0..3{
        let mut input = [0.; Decoder::I_LEN];
        input.iter_mut().for_each(|o| *o = random::<f32>() * 2. - 1. );

        let img = decoder.predict(input)?;

        matrix![&img, I![const 64, 64]].heatmap(1.)?;
    }

    let mut img_buffer = vec![0.; MEDICAL_IMAGE_SIZE];
    let category = random::<usize>() % DATA_SOURCES.len(); // Pick random category to pick image from.
    let path = format!("./medmnist/{}/{}", DATA_SOURCES[category], pick_file(100)); // Pick random image.

    let image = image::open(&path).unwrap(); // Open image and loop over all the pixels (very slow. This would be an easy pick to optimize)
    for x in 0..MEDICAL_IMAGE_WIDTH as u32 {
        for y in 0..MEDICAL_IMAGE_WIDTH as u32 {
            let v = image.get_pixel(x, y).to_luma(); // Convert pixel to grayscale.
            // Convert pixel to a float and put it in the buffer.
            img_buffer[x as usize + y as usize * MEDICAL_IMAGE_WIDTH] = v[0] as f32 / 255.;
        }
    }


    matrix![&img_buffer, I![const 64, 64]].heatmap(1.)?;
    Ok(())
}
