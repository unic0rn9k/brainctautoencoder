#![allow(incomplete_features)]
#![feature(const_generics)]

// The Yew stack will be used for the site, as it is vercitile and is based on rust, html
// javascript and can also be used along side css, and scss.
use yew::prelude::*;

// Additionaly the exotic machine learning framework will be used for the deep learning models
// used. The machine learning project was created and is maintained by unic0rn9k (Aksel) 😏
use exotic_macro::*;
use exotic::serialization::next_float; // This function takes a float from a byte iterator.

use lazy_static::*;

mod slider; // Blatantly copy'n pasted code from yew's examples folder. (used for creating html sliders)
use slider::Slider;

const MEDICAL_IMAGE_WIDTH: usize = 64; // Emojis have same height and width, therefore we don't need a const for the height.
const LATENT_FEATURES: usize = 6;

// The deep learning model needs to be re-implemented.
// This is because exotic stack allocates its model's, and the wasm stack size is too small to fit the model.
// Here vectors are used instead, as they are heap allocated.

#[derive(Clone)]
struct HeapNeuron(
    Vec<f32>, // Weights
    f32 // Bias
);

impl HeapNeuron{
    fn predict(&self, i: &[f32]) -> f32{ // This function takes the dot product of the input and the weights and adds a bias.
        i.iter().zip(self.0.iter()).map(|(i, w)| i * w).sum::<f32>() + self.1
    }

    fn from_bytes(size: usize, bytes: &mut dyn Iterator<Item = u8>) -> Self{ // This function is later used to deserialize the mebeddet byte code, that contains the pretrained model
        let mut tmp = Self(vec![], 0.);
        for _ in 0..size{
            tmp.0.push(next_float(bytes).unwrap())
        }
        tmp.1 = next_float(bytes).unwrap();
        tmp
    }
}

#[derive(Clone)]
struct Layer(Vec<HeapNeuron>);

impl Layer{
    fn predict(&self, i: &[f32]) -> Vec<f32>{
        self.0.iter().map(|n| n.predict(i)).collect() // collect predictions for each neuron into a vector.
    }

    fn from_bytes(i_size: usize, o_size: usize, bytes: &mut dyn Iterator<Item = u8>) -> Self{
        let mut tmp = Self(vec![]);
        for _ in 0..o_size{
            tmp.0.push(HeapNeuron::from_bytes(i_size, bytes))
        }
        next_float(bytes); // We need to throw away the last float, because the learning rates of the layer is also imbedded into the bytes. We only need the learning rates for training.
        tmp
    }
}

fn swish(x: f32) -> f32{ // The swish activation function is used after the second layer. And it's graph looks like a leaky relu with a soft curve near (0, 0).
        x / ((-x).exp() + 1.)
}

fn sigmoid(x: f32) -> f32 { // The sigmoid activation function is used after the output layer. The OG of activation functions.
    1. / (1. + (-x).exp())
}

#[derive(Clone)]
struct Decoder(Layer, Layer); // Our decoder contains 2 dense layers, and 2 activation functions.
                              // The activation functions don't need any data, so they will be implemted in the prediction function.

impl Decoder{
    fn new() -> Self{
        const BYTES: &[u8] = &embed_bytes!(decoder.exotic); // Enocde bytes into the binary, with a macro from the exotic_macro crate.
        let mut bytes = BYTES.iter().map(|b| *b); // Iterate over the dereferenced values of the bytes.
        Self(
            Layer::from_bytes(LATENT_FEATURES, 121, &mut bytes), // And initalize both layers...
            Layer::from_bytes(121, 4096, &mut bytes),
        )
    }

    fn predict(&self, i: &[f32]) -> Vec<f32>{
        // Iterate over prediction for the first layer, and activate each value.
        let first: Vec<f32> = self.0.predict(i).iter().map(|x| swish(*x)).collect();
        
        // Do the same for the second layer, with the output of the first, as an argument.
        self.1.predict(&first[..]).iter().map(|x| sigmoid(*x)).collect()
    }
}

lazy_static!{
    static ref DECODER: Decoder = Decoder::new();
}


// YewPage is a struct that contains the state of the page.
// This will contain all the data available on the page.
struct YewPage {
    // `ComponentLink` is like a reference to a component. (This comment was written by the yew
    // devs in one of their exaples... Amazing documentation guys 🙄)
    // It can be used to send messages to the component
    link: ComponentLink<Self>,
    is_on_front_page: bool, // We on the front page?
    sliders: [f32; LATENT_FEATURES],
    img: Vec<f32>, // Here lives the output of our neuralnet.
}

// This is a message enum.
// It will be sent to the YewPage instance every time the page needs to update.
enum Msg {
    NextPage, // Is used to signal that the user has leaft the front page.
    SetSlider(usize, f32), // Signal that user has changed a slider.
}

impl YewPage {
    fn frontpage(&self) -> Html { // HTML for the front page. This is in a separate function so the later function that calls it doesn't get too big. It's really unnecessary tho...
        html! {
            <div>
                <h1 id="title"> // This title will not be visible on the page.
                    {"This title gets overwritten by javasscript!"}
                </h1>
                         // Here we call a javascript function from rust,
                <script> // that will overwrite the title defined above, to some other text.

                     {"document.getElementById('title').textContent = 'YEET WEB APP';"}
                </script>

                <div class="description-box" align="justify">{r"
                    The Yeet web app, is a web app for making custom medical images, of brain CT scans (Computed Tomography).
                    For this an autoencoder is used, which is a type of deep learning model
                    that can compress and decompress data from and to a latent space.
                    The deep learning model consists of a encoder and a decoder.
                    The encoder takes a high dimensional vector as its input (in this case, a CT of a brain).
                    The encoder then has a low number of output neurons, that are feed to the decoder.
                    After the decoder has received the encoded input, it will try to reproduce the original input, that the encoder received.
                    This way the neuralnet ('s) learns to find out what information in the input is relevant, and how to represent it with a small amount of information.
                "}</div>

                <h3>{"What is a latent space?"}</h3>

                <div class="description-box" align="justify">{r"
                    A latent space is a type of vector encoding where each dimension of the vector corresponds to a quantifiable feature.
                    For example if a neural net is trained on images of cats,
                    it might end up with a dimension in the encoded latent space that corresponds to the color of the cats fur.
                "}</div>

                <h3>{"What does this have to do with brain's?"}</h3>

                <div class="description-box" align="justify">{r"
                    The yeet web app contains a model that has been pretrained on a large dataset of brains (also done by us).
                    The app only contains the decoder which is used to produce brains based on user specifications.
                    The end result is that the user can adjust some sliders corresponding to features in the resulting brains,
                    and thus make their own brains. This can be very useful in a learning scenario, as medical data is often hard to come by because of data protection laws.
                    But with this algorithm you can generate images of brains with just the disease you're looking for showing your students as an example.
                    Links bellow to try it, and sources.
                "}</div>

                <button onclick=self.link.callback(|_|Msg::NextPage) class="fake-link">{"Try it out!"}</button>
                <a href="https://www.kaggle.com/andrewmvd/medical-mnist">{"Data set used"}</a>
            </div>
        }
    }
}

// CSS used for rendering a "pixel" on screen, which is a div block with a solid color and fixed
// size. This will be used for rednering images with only html and css later.
// Note: anything less than 5px looks weird.
const PIXEL_STYLE: &str = r"
  display: inline-block;
  width: 5px;
  height: 5px;
  border: none;
";

// Returns html and css for rednering a single pixel.
fn pixel(color: &str) -> Html {
    html! {
        <div style=format!("{} background-color: {};", PIXEL_STYLE, color)></div>
    }
}

impl Component for YewPage {
    type Message = Msg;
    type Properties = ();

    // To implement the yew component trait (which is is used for rendering the page), the YewPage
    // struct needs to be initializable.
    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            is_on_front_page: true,
            link,
            sliders: [0.; LATENT_FEATURES],
            img: DECODER.predict(&[0.; LATENT_FEATURES]),
        }
    }

    // This function is called by yew every time the page is updated, and takes an additional
    // argument, which is of the message type, defined in the top of the trait (in this case it is the
    // Msg enum).
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
           Msg::NextPage => {
                self.is_on_front_page = false; // Set to false, so the view function changes the page
                true // Return true, so yew knows to re-render the page.
            }
            Msg::SetSlider(index, value) => { // ...You know what it does...
                self.sliders[index] = value;
                self.img = DECODER.predict(&self.sliders); // Write output of neuralnet to img
                true
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    // This function should return the html for the page.
    // The html macro (from yew), is used to write html code directly in rust.
    fn view(&self) -> Html {
        if self.is_on_front_page{
            // Return the html for the frontpage, if the user hasn't gone to the next page. 
            html!{<center>{self.frontpage()}</center>}
        }else{
            let cell_rows = self.img
                .chunks(MEDICAL_IMAGE_WIDTH) // The chunk method returns an array of ellemnts, this lets is get the rows of the render that needs to be displayed. 
                .map(|pixel_row| { // Iterate over rows of pixels
                    let pixel_row = pixel_row
                        .iter()
                        .map(|f|format!("#{}",format!("{:x?}", (f * 255.) as u8).repeat(3)))
                        .map(|cell| pixel(&cell)); // Conver pixel from float to an html color code, 
                                                   // and map all pixel to their div tags.
                    html! {
                        <div class="canvas-row"> // Div block for a row of pixels,
                                                 // which need a css tag to remove
                                                 // spacing between lines.

                            { for pixel_row }    // repeat for all rows.
                        </div>
                    }
                });

            const SLIDER_INFO: [&'static str; LATENT_FEATURES] = [ // Text to put above sliders
                "Back size",
                "Angle",
                "Front length",
                "Roundness",
                "Eyes and nose",
                "Depth",
            ];

            let sliders = (0..self.sliders.len())
                .map(|n|html!{<div>
                    {SLIDER_INFO[n]}
                    <Slider label=""
                        max=1.
                        min=-1.
                        step=0.002
                        onchange=self.link.callback(move |value| Msg::SetSlider(n, value)) // Did we change a slider? Then call this closure (function pointer).
                        value=self.sliders[n] // Value to set the slider to.
                    /></div>
                });

            html!{
                <div class="description-box" align="center">
                    <h1>{"YEET"}</h1>
                    <h3>{"This brain does not exist..."}</h3>
                    <div>
                        <section class="canvas">
                            { for cell_rows }   // Collect all pixels onto the canvas section defined in our css.
                        </section>
                    </div> <br/>
                    <div>
                        { for sliders } // Put slider here...
                    </div>
                </div>
            }
        }
    }
}

// The main function runs the yew app with the YewPage as a generic argument, which has to
// implement the yew component trait (which was done above).
fn main() {
    yew::start_app::<YewPage>();
}
